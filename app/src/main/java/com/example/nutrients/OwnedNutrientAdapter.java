package com.example.nutrients;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class OwnedNutrientAdapter extends RecyclerView.Adapter<OwnedNutrientAdapter.NutrientViewHolder> {

    private List<Nutrient> ownedNutrients;
    private final Context context;
    private final NutrientClickListener nutrientClickListener;

    public OwnedNutrientAdapter(Context context, NutrientClickListener nutrientClickListener) {
        this.context = context;
        this.ownedNutrients = new ArrayList<>();
        this.nutrientClickListener = nutrientClickListener;
    }

    @NonNull
    @Override
    public NutrientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new NutrientViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull NutrientViewHolder holder, int position) {
        Nutrient nutrient = ownedNutrients.get(position);
        if (nutrient != null) {
            holder.tvName.setText(nutrient.getName());
            holder.tvSources.setText(nutrient.getSources());
            holder.tvDeficiency.setText(nutrient.getDeficiency());
        }
    }

    @Override
    public int getItemCount() {
        return this.ownedNutrients.size();
    }

    public void setOwnedNutrients(List<Nutrient> ownedNutrients) {
        this.ownedNutrients = new ArrayList<>(ownedNutrients);
        notifyDataSetChanged();
    }

    public void addNutrient(Nutrient nutrient) {
        ownedNutrients.add(nutrient);
        notifyItemInserted(ownedNutrients.indexOf(nutrient));
    }

    public List<Nutrient> getNutrients() {
        return this.ownedNutrients;
    }

    public void removeNutrient(int position) {
        this.ownedNutrients.remove(position);
        notifyItemRemoved(position);
    }

    public Nutrient getNutrient(int position) {
        return this.ownedNutrients.get(position);
    }

    public interface NutrientClickListener {
        void onButtonClick(int position);
        void onViewClick(int position);
    }

    class NutrientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView tvName;
        final TextView tvSources;
        final TextView tvDeficiency;
        final ImageButton btnDelete;

        public NutrientViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvSources = itemView.findViewById(R.id.tvSources);
            tvDeficiency = itemView.findViewById(R.id.tvDeficiency);
            btnDelete = itemView.findViewById(R.id.imgBtn);
            btnDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.getId()==R.id.imgBtn){
                nutrientClickListener.onButtonClick(getAdapterPosition());
            }
            else{
                nutrientClickListener.onViewClick(getAdapterPosition());
            }
        }
    }
}
