package com.example.nutrients;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.nutrients.room.AppDatabase;
import com.example.nutrients.room.AppExecutors;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class AvailableNutrients extends AppCompatActivity implements AvailableNutrientAdapter.AvailableNutrientClickListener {
    RecyclerView mAvailableNutrientsRecycler;
    AvailableNutrientAdapter mAvailableNutrientAdapter;
    List<Nutrient> mAvailableNutrients = new ArrayList<>();
    LiveData<List<Nutrient>> mNutrientsLiveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_AvailableNutrients);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_add_layout);
        setUpRecycler();
        setUpData();
    }

    private void setUpRecycler() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mAvailableNutrientsRecycler = findViewById(R.id.dialog_recycler);
        mAvailableNutrientsRecycler.setLayoutManager(layoutManager);
        mAvailableNutrientsRecycler.setHasFixedSize(true);

        mAvailableNutrientAdapter = new AvailableNutrientAdapter(this, this);
        mAvailableNutrientsRecycler.setAdapter(mAvailableNutrientAdapter);
        mAvailableNutrientAdapter.setAvailableNutrients(mAvailableNutrients);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mAvailableNutrientsRecycler.getContext(),
                layoutManager.getOrientation());
        mAvailableNutrientsRecycler.addItemDecoration(dividerItemDecoration);

    }

    private void setUpData() {
        AppDatabase mDb = AppDatabase.getInstance(this);
        mNutrientsLiveData = mDb.nutrientDao().getAvailableNutrients();

        // Wait for database to fill with data
        mNutrientsLiveData.observe(this, new Observer<List<Nutrient>>() {
            @Override
            public void onChanged(List<Nutrient> nutrientList) {

                mAvailableNutrients = nutrientList;

                runOnUiThread(() -> {
                    // Fill adapter with data

                    mAvailableNutrientAdapter.setAvailableNutrients(mAvailableNutrients);
                });

                if (nutrientList.size() > 0) {
                    mNutrientsLiveData.removeObserver(this);
                    mNutrientsLiveData = null;
                }
            }
        });
    }


    @Override
    public void onButtonClick(int position) {
        if (position != -1) {
            Nutrient nutrient = mAvailableNutrientAdapter.getNutrient(position);
            nutrient.setOwned(this);
            mAvailableNutrientAdapter.removeNutrient(position);
            mAvailableNutrients.remove(position);
        }
    }

    @Override
    public void onViewClick(int position) {
        Intent intent = new Intent(this, DetailNutrientView.class);
        if(position!=-1){
            int nutrientID = mAvailableNutrientAdapter.getNutrient(position).getId();
            intent.putExtra("nutrientID",nutrientID);
            startActivity(intent);
        }
    }
}