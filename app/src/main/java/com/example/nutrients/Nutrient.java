package com.example.nutrients;

import android.content.Context;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.nutrients.room.AppDatabase;
import com.example.nutrients.room.AppExecutors;

@Entity(tableName = "nutrients")
public class Nutrient {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String description;
    private String sources;
    private String deficiency;
    private int owned;

    public Nutrient(String name, String description, String sources, String deficiency, int owned) {
        this.name = name;
        this.description = description;
        this.sources = sources;
        this.deficiency = deficiency;
        this.owned = owned;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

    public String getDeficiency() {
        return deficiency;
    }

    public void setDeficiency(String deficiency) {
        this.deficiency = deficiency;
    }

    public int getOwned() {
        return owned;
    }

    public boolean isOwned() {
        return owned > 0;
    }

    public void setOwned(Context context) {
        this.owned = 1;
        AppDatabase mDb = AppDatabase.getInstance(context);
        AppExecutors.getInstance().diskIO().execute(() -> mDb.nutrientDao().update(Nutrient.this));
    }

    public void unOwn(Context context) {
        this.owned = 0;
        AppDatabase mDb = AppDatabase.getInstance(context);
        AppExecutors.getInstance().diskIO().execute(() -> mDb.nutrientDao().update(Nutrient.this));
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
