package com.example.nutrients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class AvailableNutrientAdapter extends RecyclerView.Adapter<AvailableNutrientAdapter.AvailableNutrientViewHolder> {

    private final Context context;
    private List<Nutrient> availableNutrients;
    private final AvailableNutrientClickListener availableNutrientClickListener;

    public AvailableNutrientAdapter(Context context, AvailableNutrientClickListener availableNutrientClickListener) {
        this.context = context;
        this.availableNutrients = new ArrayList<>();
        this.availableNutrientClickListener = availableNutrientClickListener;
    }

    @NonNull
    @Override
    public AvailableNutrientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new AvailableNutrientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableNutrientViewHolder holder, int position) {
        Nutrient nutrient = availableNutrients.get(position);
        if (nutrient != null) {
            holder.tvName.setText(nutrient.getName());
            holder.tvSources.setText(nutrient.getSources());
            holder.tvDeficiency.setText(nutrient.getDeficiency());
        }
    }

    @Override
    public int getItemCount() {
        return availableNutrients.size();
    }

    public void removeNutrient(int position) {
        this.availableNutrients.remove(position);
        notifyItemRemoved(position);
    }

    public interface AvailableNutrientClickListener {
        void onButtonClick(int position);
        void onViewClick(int position);
    }

    public void setAvailableNutrients(List<Nutrient> availableNutrients) {
        this.availableNutrients = new ArrayList<>(availableNutrients);
        notifyDataSetChanged();
    }

    public Nutrient getNutrient(int position) {
        return availableNutrients.get(position);
    }

    public void addNutrient(Nutrient nutrient) {
        availableNutrients.add(nutrient);
        notifyItemInserted(availableNutrients.indexOf(nutrient));
    }


    class AvailableNutrientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView tvName;
        final TextView tvSources;
        final TextView tvDeficiency;
        final ImageButton btnAdd;

        public AvailableNutrientViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvSources = itemView.findViewById(R.id.tvSources);
            btnAdd = itemView.findViewById(R.id.imgBtn);
            tvDeficiency = itemView.findViewById(R.id.tvDeficiency);
            btnAdd.setImageResource(R.drawable.ic_baseline_add_circle_outline_24);
            btnAdd.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if(view.getId()==R.id.imgBtn){
                availableNutrientClickListener.onButtonClick(getAdapterPosition());
            }
            else{
                availableNutrientClickListener.onViewClick(getAdapterPosition());
            }

        }
    }
}
