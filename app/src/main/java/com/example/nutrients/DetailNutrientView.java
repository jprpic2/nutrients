package com.example.nutrients;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.nutrients.room.AppDatabase;
import com.example.nutrients.room.AppExecutors;

import org.w3c.dom.Text;

public class DetailNutrientView extends AppCompatActivity {
    TextView name;
    TextView desc;
    TextView sources;
    TextView deficiency;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_nutrient_view);
        Intent intent = getIntent();
        int nutrientID = intent.getIntExtra("nutrientID",-1);
        setUpViews();
        setUpData(nutrientID);
    }

    private void setUpData(int nutrientID) {
        AppDatabase mDb = AppDatabase.getInstance(this);
        AppExecutors.getInstance().diskIO().execute(() -> {
            Nutrient nutrient = mDb.nutrientDao().getNutrient(nutrientID);
            runOnUiThread(() -> {
                name.setText(nutrient.getName());
                desc.setText(nutrient.getDescription());
                sources.setText(nutrient.getSources());
                deficiency.setText(nutrient.getDeficiency());
            });
        });
    }

    private void setUpViews() {
        name = findViewById(R.id.tvDetailName);
        desc = findViewById(R.id.tvDetails);
        sources = findViewById(R.id.tvDetailSources);
        deficiency = findViewById(R.id.tvDetailDeficiency);
    }
}