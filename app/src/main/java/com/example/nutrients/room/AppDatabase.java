package com.example.nutrients.room;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.nutrients.Nutrient;

@Database(entities = {Nutrient.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "nutrient_data.db";
    private static AppDatabase mDb;

    public static AppDatabase getInstance(Context context) {
        if (mDb == null) {
            synchronized (LOCK) {
                Log.d("db", "Creating new database instance");
                mDb = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .addCallback(new Callback() {
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);
                                AppExecutors.getInstance().diskIO().execute(() -> populateDatabase(getInstance(context)));
                            }
                        })
                        .build();
            }
        }
        return mDb;
    }

    private static void populateDatabase(AppDatabase instance) {
        instance.nutrientDao().insertAll(NutrientData.populateNutrients());
    }

    public abstract NutrientDao nutrientDao();
}
