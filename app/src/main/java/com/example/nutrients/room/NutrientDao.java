package com.example.nutrients.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.nutrients.Nutrient;

import java.util.List;

@Dao
public interface NutrientDao {
    @Insert
    void insertAll(Nutrient... obj);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Nutrient obj);

    @Query("SELECT * from nutrients WHERE owned = 1")
    LiveData<List<Nutrient>> getOwnedNutrients();

    @Query("SELECT * from nutrients WHERE owned = 0")
    LiveData<List<Nutrient>> getAvailableNutrients();

    @Query("SELECT * from nutrients WHERE id = :nutrientID")
    Nutrient getNutrient(int nutrientID);

}
