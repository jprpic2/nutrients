package com.example.nutrients.room;

import com.example.nutrients.Nutrient;

public class NutrientData {
    public static Nutrient[] populateNutrients() {
        return new Nutrient[]{
                new Nutrient("Vitamin A","Desc","Jaja, mliječni proizvodi, pileća i goveđa jetra, zeleno i lisnato povrće","Noćna sljepoća, oslabljeni imunološki sustav, disfunkcija štitnjače",0),
                new Nutrient("Vitamin B1","Desc","Govedina, svinjetina, perad, cjelovite žitarice, smeđa riža, orašasti plodovi ","Umor, depresija, iritacija, mućnost, glavobolje, nelagoda u trbuhu",0),
                new Nutrient("Vitamin B2","Desc","Pivski kvasac, divlja riža, cjelovite žitarice, bademi, mlijeko, gljive, jogurt, brokula","Crvenilo i oticanje grla i usta, grlobolja, rane i pukotine u kutevima usana",0),
                new Nutrient("Vitamin B3","Desc","Goveđi bubreg, goveđa jetra, tuna, sabljarka, losos, pivski kvasac, cikla, kikiriki","Umor, loša cirkulacija, depresija, povraćanje, probavne smetnje, afte",0),
                new Nutrient("Vitamin B5","Desc","Karfiol, kukuruz, pivski kvasac, kelj, rajčica, brokula, avokado, piletina, puretina, kikiriki","Nesanica, umor, depresija, iritacija, povraćanje, senzacija vrućih stopala",0),
                new Nutrient("Vitamin B6","Desc","Goveđa jetra, puretina, piletina, tuna, losos, sir, mlijeko, grah, leća, mrkva, špinat, banane","Nervoza, oslabljenost mišića, depresija, iritacija, kratkotrajni gubitak pamćenja",0),
                new Nutrient("Vitamin B9","Desc","Kelj pupčar, tamno lisnato povrće, cikla, grah, losos, avokado, cjelovite žitarice, sok od naranče","Upala jezika, upala desni, nedostatak apetita, proljev, nedostatak disanja",0),
                new Nutrient("Vitamin B12","Desc","Školjke, riba, svinjetina, govedina, jaja, mliječni proizvodi","Trnci u prstima na rukama i nogama, umor, nervoza, proljev, nedostatak disanja",0),
                new Nutrient("Vitamin C","Desc","Jagode, maline, borovnice, brusnica, naranča, kiwi, rajčica, lisnato povrće, brokula, karfiol","Upala desni, suha kosa, ljuskava koža, sporije zacijeljenje rana, lagano dobivanje modrica, krvarenja iz nosa",0),
                new Nutrient("Vitamin D","Desc","Mlijeko s pojačanim vitaminom D, sunce","Rahitis",0),
                new Nutrient("Vitamin E","Desc","Pšenične klice, sjemenke suncokreta, orašasti plodovi, jaja, maslinovo ulje, špinat, avokado","Problemi s vidom, slabost mišića, nestabilno hodanje, problemi s jetrom i bubrezima",0),
                new Nutrient("Vitamin K","Desc","Zeleni čaj, kelj, brokula, kupus, špinat, zelena salata, šparoga","Prekomjerno krvarenje",0),
                new Nutrient("Fosfor","Desc","Krhke kosti, nedostatak apetita, ukočeni zglobovi, nepravilno disanje, umor, iritacija","Meso, riba, perad, mliječni proizvodi, jaja, češnjak, orašasti plodovi",0),
                new Nutrient("Jod","Desc","Bijela morska riba, kamenice, sjemenke sezama, češnjak, špinat","Povećana štitnjača, niski hormoni štitnjače, suha koža, debljanje, umor",0),
                new Nutrient("Kalcij","Desc","Pivski kvasac, sir, jogurt, mlijeko, tofu, orašasti plodovi, kupus, brokula, sardine, kamenice","Grčenje mišića, lomljivi nokti, gubitak pamćenja, zbunjenost, trnci u rukama, nogama i licu",0),
                new Nutrient("Kalij","Desc","Citrusi, banane, avokado, rajčica, krumpir, riba, piletina, crveno meso","Grčevi mišića nogu i ruku, umor, muka ili povraćanje, zatvor, oticanje, pretjerano uriniranje ili žeđ",0),
                new Nutrient("Krom","Desc","Nemasno meso, pivski kvasac, sir, određeni začini, proizvodi od cjelovitih žitarica","Povećani šećer u krvi i kolesterol, povećani rizik od dijabetesa i srčanih bolesti",0),
                new Nutrient("Magnezij","Desc","Tofu, cjelovite žitarice, zeleno lisnato povrće, bundeva, mak, banane, tamna čokolada","Problemi sa spavanjem, sindrom nemirnih nogu, iritacija, anksioznost, muka, povraćanje, nizak krvni tlak",0),
                new Nutrient("Selenij","Desc","Pivski kvasac, tuna, iverak, skuša, haringa, jastog, Jakobove kapice, maslac, češnjak","Umor, neplodnost, pojačano djelovanje određenih virusa kod infekcije",0),
                new Nutrient("Željezo","Desc","Crveno meso, riba, perad, orašasti plodovi, zeleno lisnato povrće, sjemenke","Anemija, umor, slabost",0),

        };
    }
}
