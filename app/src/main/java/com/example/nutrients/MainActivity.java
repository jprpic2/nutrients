package com.example.nutrients;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutrients.room.AppDatabase;
import com.example.nutrients.room.AppExecutors;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OwnedNutrientAdapter.NutrientClickListener {

    private RecyclerView mOwnedNutrientRecycler;
    private List<Nutrient> mOwnedNutrients = new ArrayList<>();
    private OwnedNutrientAdapter mOwnedNutrientAdapter;
    private TextView emptyRecycler;
    private LiveData<List<Nutrient>> mNutrientsLiveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emptyRecycler = findViewById(R.id.empty_recylcer_tv);

        InitializeRecyclers();
        setUpFab();
    }

    private void setUpFab() {
        FloatingActionButton fab = findViewById(R.id.fabAdd);

        fab.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, AvailableNutrients.class);
            startActivity(intent);
        });
    }


    private void InitializeRecyclers() {
        mOwnedNutrientRecycler = findViewById(R.id.recycler);

        mOwnedNutrientAdapter = new OwnedNutrientAdapter(this, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mOwnedNutrientRecycler.setLayoutManager(layoutManager);
        mOwnedNutrientRecycler.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mOwnedNutrientRecycler.getContext(),
                layoutManager.getOrientation());
        mOwnedNutrientRecycler.addItemDecoration(dividerItemDecoration);
        mOwnedNutrientRecycler.setAdapter(mOwnedNutrientAdapter);
    }


    private void setUpData() {
        AppDatabase mDb = AppDatabase.getInstance(this);
        mNutrientsLiveData = new LiveData<List<Nutrient>>(){};
        mNutrientsLiveData = mDb.nutrientDao().getOwnedNutrients();

        // Wait for database to fill with data
        mNutrientsLiveData.observe(this, new Observer<List<Nutrient>>() {
            @Override
            public void onChanged(List<Nutrient> nutrientList) {

                mOwnedNutrients = nutrientList;

                runOnUiThread(() -> {
                    // Fill adapter with data

                    mOwnedNutrientAdapter.setOwnedNutrients(mOwnedNutrients);
                    updateEmptyLayout();
                });

                // After first callback stop observing

                if (nutrientList.size() > 0 && mNutrientsLiveData.hasObservers()) {
                    mNutrientsLiveData.removeObserver(this);
                    mNutrientsLiveData = null;
                }
            }
        });
    }

    private void updateEmptyLayout() {
        if (mOwnedNutrientAdapter.getItemCount() > 0) {
            emptyRecycler.setVisibility(View.INVISIBLE);
            mOwnedNutrientRecycler.setVisibility(View.VISIBLE);
        } else {
            emptyRecycler.setVisibility(View.VISIBLE);
            mOwnedNutrientRecycler.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStart() {
        setUpData();
        super.onStart();
    }

    @Override
    public void onButtonClick(int position) {
        if (position != -1) {
            Nutrient nutrient = mOwnedNutrientAdapter.getNutrient(position);
            nutrient.unOwn(this);
            mOwnedNutrientAdapter.removeNutrient(position);
            mOwnedNutrients.remove(position);
            updateEmptyLayout();
        }
    }

    @Override
    public void onViewClick(int position) {
        Intent intent = new Intent(this, DetailNutrientView.class);
        if(position!=-1){
            int nutrientID = mOwnedNutrientAdapter.getNutrient(position).getId();
            intent.putExtra("nutrientID",nutrientID);
            startActivity(intent);
        }
    }
}